package com.example.cursoSpringBootEpet4.repositorios;

import com.example.cursoSpringBootEpet4.modelo.AlumnoModelo;
import com.example.cursoSpringBootEpet4.modelo.ItemVenta;
import com.example.cursoSpringBootEpet4.modelo.Venta;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ItemVentaRepositorio  extends JpaRepository<ItemVenta, Long> {
    List<ItemVenta> findByEstado(Long estado);
    List<ItemVenta> findByEstadoAndVenta(Long estado, Venta venta);
}
