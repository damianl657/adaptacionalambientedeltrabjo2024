package com.example.cursoSpringBootEpet4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CursoSpringBootEpet4Application {

	public static void main(String[] args) {
		SpringApplication.run(CursoSpringBootEpet4Application.class, args);
	}

}
