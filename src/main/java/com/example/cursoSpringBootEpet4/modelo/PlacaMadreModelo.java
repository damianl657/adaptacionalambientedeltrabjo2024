package com.example.cursoSpringBootEpet4.modelo;

import jakarta.persistence.*;

@Entity
@Table(name="placa_madre")
public class PlacaMadreModelo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;
    private String socket;
    private String marca;
    private String modelo;
    private int cantidadBancosMemorias;
    private String tipoMemoria;
    private int cantidadBancosPCIExpress;

    private Long estado;

    public Long getEstado() {
        return estado;
    }

    public void setEstado(Long estado) {
        this.estado = estado;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSocket() {
        return socket;
    }

    public void setSocket(String socket) {
        this.socket = socket;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getCantidadBancosMemorias() {
        return cantidadBancosMemorias;
    }

    public void setCantidadBancosMemorias(int cantidadBancosMemorias) {
        this.cantidadBancosMemorias = cantidadBancosMemorias;
    }

    public String getTipoMemoria() {
        return tipoMemoria;
    }

    public void setTipoMemoria(String tipoMemoria) {
        this.tipoMemoria = tipoMemoria;
    }

    public int getCantidadBancosPCIExpress() {
        return cantidadBancosPCIExpress;
    }

    public void setCantidadBancosPCIExpress(int cantidadBancosPCIExpress) {
        this.cantidadBancosPCIExpress = cantidadBancosPCIExpress;
    }
}
