package com.example.cursoSpringBootEpet4.repositorios;
import com.example.cursoSpringBootEpet4.modelo.ClienteModelo;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
public interface ClienteRepositorio extends JpaRepository<ClienteModelo, Long> {
    List<ClienteModelo> findByEstado(Long estado);
    List<ClienteModelo> findByEstadoOrderByNombreAsc(Long estado);
}
