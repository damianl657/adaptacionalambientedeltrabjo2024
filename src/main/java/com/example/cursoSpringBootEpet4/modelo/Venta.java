package com.example.cursoSpringBootEpet4.modelo;

import jakarta.persistence.*;
import org.hibernate.annotations.CreationTimestamp;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="venta")
public class Venta {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;
    private Long estado;
    @CreationTimestamp
    private Date fecha;
    private Double total;
    @ManyToOne
    @JoinColumn(name="cliente_id")
    private ClienteModelo cliente;

    @OneToMany( mappedBy = "venta")
    private List<ItemVenta> itemVentas = new ArrayList<>();

    public List<ItemVenta> getItemVentas() {
        return itemVentas;
    }

    public void setItemVentas(List<ItemVenta> itemVentas) {
        this.itemVentas = itemVentas;
    }

    public ClienteModelo getCliente() {
        return cliente;
    }

    public void setCliente(ClienteModelo cliente) {
        this.cliente = cliente;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEstado() {
        return estado;
    }

    public void setEstado(Long estado) {
        this.estado = estado;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }
}
