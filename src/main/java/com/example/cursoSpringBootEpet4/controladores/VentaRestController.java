package com.example.cursoSpringBootEpet4.controladores;

import com.example.cursoSpringBootEpet4.modelo.ItemVenta;
import com.example.cursoSpringBootEpet4.modelo.Venta;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api-venta")
public class VentaRestController {
    @PostMapping("/alta")
    public String alta(@RequestBody Venta venta){
        System.out.println("cli: "+venta.getCliente().getId());
        System.out.println("tamaño prod: "+venta.getItemVentas().size());
        for(ItemVenta itemVenta: venta.getItemVentas()){
            System.out.println("id prod: "+itemVenta.getProducto().getId());
        }
        return "redirect:/venta";
    }
}
