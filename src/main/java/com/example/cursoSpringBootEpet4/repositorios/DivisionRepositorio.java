package com.example.cursoSpringBootEpet4.repositorios;

import com.example.cursoSpringBootEpet4.modelo.DivisionModelo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DivisionRepositorio extends JpaRepository<DivisionModelo,Long> {
}
