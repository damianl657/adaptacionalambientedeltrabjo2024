package com.example.cursoSpringBootEpet4.repositorios;


import com.example.cursoSpringBootEpet4.modelo.Precio;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PrecioRepositorio  extends JpaRepository<Precio, Long> {
    List<Precio> findByEstado(Long estado);
}
