package com.example.cursoSpringBootEpet4.servicios;

import com.example.cursoSpringBootEpet4.modelo.DivisionModelo;
import com.example.cursoSpringBootEpet4.repositorios.DivisionRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DivisionServicio {
    @Autowired
    DivisionRepositorio divisionRepositorio;

    public List<DivisionModelo> listarTodos(){
        return divisionRepositorio.findAll();
    }

    public DivisionModelo obtenerDivisionPorId(Long id){
        return divisionRepositorio.findById(id).get();
    }
}