package com.example.cursoSpringBootEpet4.controladores;

import com.example.cursoSpringBootEpet4.modelo.*;
import com.example.cursoSpringBootEpet4.repositorios.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/venta")
public class VentaControlador {
    @Autowired
    private ClienteRepositorio clienteRepositorio;
    @Autowired
    private VentaRepositorio ventaRepositorio;
    @Autowired
    private ItemVentaRepositorio itemVentaRepositorio;
    @Autowired
    private PrecioRepositorio precioRepositorio;
    @Autowired
    private ProductoRepositorio productoRepositorio;
    @Autowired
    private MarcaRepositorio marcaRepositorio;
    @GetMapping({"","/"})
    public String principal(Model modelo){
        List<Venta> ventaArrayList = new ArrayList<>();
        ventaArrayList = ventaRepositorio.findByEstado(1L);
        modelo.addAttribute("ventas",ventaArrayList);
        return "venta/principal";
    }
    @GetMapping("/items-venta/{id}")
    public String items(@PathVariable Long id, Model modelo){
        Venta venta = this.ventaRepositorio.findById(id).get();
        List<ItemVenta> itemVentas = this.itemVentaRepositorio.findByEstadoAndVenta(1L,venta);
        modelo.addAttribute("venta",venta);
        modelo.addAttribute("itemVentas",itemVentas);
        return "venta/items";
    }
    @GetMapping("/alta")
    public String alta(Model modelo){
        Venta venta = new Venta();
        List<ClienteModelo> clientes = new ArrayList<>();
        List<Producto> productos = new ArrayList<>();
        productos = this.productoRepositorio.findByEstado(1L);
        clientes = this.clienteRepositorio.findByEstado(1L);
        modelo.addAttribute("venta",venta);
        modelo.addAttribute("clientes",clientes);
        modelo.addAttribute("productos",productos);
        return "venta/alta";
    }

}
