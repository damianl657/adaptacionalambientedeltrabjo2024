package com.example.cursoSpringBootEpet4.controladores;

import com.example.cursoSpringBootEpet4.utilidades.Auto;
import com.example.cursoSpringBootEpet4.utilidades.PlacaMadre;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.LinkedList;
import java.util.List;

@Controller
@RequestMapping("/poo")
public class PracticaPoo {

    @GetMapping("/practicaProfe1")
    public String practicaProfe1(Model modelo){
        System.out.println("-------------------");
        PlacaMadre placaMadre = new PlacaMadre();
        placaMadre.setMarca("Gigbyte");
        placaMadre.setCantidadBancosMemorias(4);
        placaMadre.setModelo("Z690");
        placaMadre.setCantidadBancosPCIExpress(1);
        placaMadre.setSocket("1700");
        placaMadre.setTipoMemoria("DDR5");




        System.out.println("Datos Placa Madre:");
        System.out.println("Marca: "+placaMadre.getMarca());
        System.out.println("Modelo: "+placaMadre.getModelo());

        PlacaMadre placaMadre2 = new PlacaMadre();
        placaMadre2.setTipoMemoria("DDR5");
        placaMadre2.setMarca("Asus");
        placaMadre2.setModelo("Z888");
        placaMadre2.setSocket("1700");
        placaMadre2.setCantidadBancosPCIExpress(2);
        System.out.println("Datos Placa Madre2:");
        System.out.println("Marca: "+placaMadre2.getMarca());
        System.out.println("Modelo: "+placaMadre2.getModelo());
        System.out.println("Tipo Memoria: "+placaMadre2.getTipoMemoria());
        return "practicajava/practica1";
    }

    @GetMapping("/practicaAlumno")
    public String practicaAlumno(Model modelo){
        //no borrar este renglon. ES UN SEPARADOR PARA LA CONSOLA
        System.out.println("-------------------");
        Auto auto1 = new Auto();
        auto1.setMarca("Peugeot");
        auto1.setModelo("408");
        auto1.setAnioFabricacion(2017);
        System.out.println("Marca: "+auto1.getMarca());

        return "practicajava/practica1";
    }
    /*LISTAS EN JAVA*/
    @GetMapping("/practicaProfe2")
    public String practicaProfe2(Model modelo){
        //no borrar este renglon. ES UN SEPARADOR PARA LA CONSOLA
        System.out.println("-------------------");
        PlacaMadre placaMadre = new PlacaMadre();
        placaMadre.setMarca("Gigbyte");
        placaMadre.setCantidadBancosMemorias(4);
        placaMadre.setModelo("Z690");
        placaMadre.setCantidadBancosPCIExpress(1);
        placaMadre.setSocket("1700");
        placaMadre.setTipoMemoria("DDR5");

        PlacaMadre placaMadre2 = new PlacaMadre();
        placaMadre2.setMarca("Gigbyte");
        placaMadre2.setCantidadBancosMemorias(4);
        placaMadre2.setModelo("Z690");
        placaMadre2.setCantidadBancosPCIExpress(1);
        placaMadre2.setSocket("1700");
        placaMadre2.setTipoMemoria("DDR5");

        PlacaMadre placaMadre3 = new PlacaMadre();
        placaMadre3.setMarca("Gigbyte");
        placaMadre3.setCantidadBancosMemorias(4);
        placaMadre3.setModelo("Z690");
        placaMadre3.setCantidadBancosPCIExpress(1);
        placaMadre3.setSocket("1700");
        placaMadre3.setTipoMemoria("DDR5");

        List<PlacaMadre> placaMadreList = new LinkedList();
        placaMadreList.add(placaMadre);
        placaMadreList.add(placaMadre2);
        placaMadreList.add(placaMadre3);

        System.out.println("Cantidad de elementos de la lista son: "+placaMadreList.stream().count());
        for (int i = 0; i < placaMadreList.stream().count(); i++){
            System.out.println("Datos Placa Madre N°"+(i+1));
            System.out.println("Marca: "+placaMadreList.get(i).getMarca());
            System.out.println("Modelo: "+placaMadreList.get(i).getModelo());
        }
        return "practicajava/practica1";
    }
    //LISTAS PRACTICA ALUMNOS
    @GetMapping("/practicaAlumno2")
    public String practicaAlumno2(Model modelo){
        //no borrar este renglon. ES UN SEPARADOR PARA LA CONSOLA
        System.out.println("-------------------");
        return "practicajava/practica1";
    }
}

