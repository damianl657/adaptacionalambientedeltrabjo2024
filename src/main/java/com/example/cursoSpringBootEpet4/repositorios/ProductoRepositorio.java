package com.example.cursoSpringBootEpet4.repositorios;

import com.example.cursoSpringBootEpet4.modelo.Producto;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductoRepositorio  extends JpaRepository<Producto, Long> {
    List<Producto> findByEstado(Long estado);
}
