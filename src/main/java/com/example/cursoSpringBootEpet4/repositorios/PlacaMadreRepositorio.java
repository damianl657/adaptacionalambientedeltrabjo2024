package com.example.cursoSpringBootEpet4.repositorios;

import com.example.cursoSpringBootEpet4.modelo.AlumnoModelo;
import com.example.cursoSpringBootEpet4.modelo.PlacaMadreModelo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlacaMadreRepositorio extends JpaRepository<PlacaMadreModelo,Long> {
    List<PlacaMadreModelo> findByEstado(Long estado);
}
