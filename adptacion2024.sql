-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 29-08-2024 a las 00:09:08
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 7.4.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `adptacion2024`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumnos`
--

DROP TABLE IF EXISTS `alumnos`;
CREATE TABLE IF NOT EXISTS `alumnos` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `apellido` varchar(255) DEFAULT NULL,
  `dni` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `estado` bigint(20) DEFAULT NULL,
  `genero` varchar(255) DEFAULT NULL,
  `matricula` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `division_id` bigint(20) DEFAULT NULL,
  `division_modelo_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKjx0qmy2g2c81yaja4ca9cnoli` (`division_id`),
  KEY `FKhwsgk51d3cggvbaawwaba4asi` (`division_modelo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `alumnos`
--

INSERT INTO `alumnos` (`id`, `apellido`, `dni`, `email`, `estado`, `genero`, `matricula`, `nombre`, `division_id`, `division_modelo_id`) VALUES
(1, 'leal', '7777777', 'damianl657@gmail.com', 1, 'M', '5454', 'damian', 2, NULL),
(2, 'alguien 12', '23232332', 'damianl657222@gmail.com', 1, 'M', '23323', 'alguien', 2, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

DROP TABLE IF EXISTS `cliente`;
CREATE TABLE IF NOT EXISTS `cliente` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `apellido` varchar(255) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `dni` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `estado` bigint(20) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  `genero_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK2jneo30tbxtors16jegervd50` (`genero_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id`, `apellido`, `direccion`, `dni`, `email`, `estado`, `nombre`, `telefono`, `genero_id`) VALUES
(1, 'pepa', 'lejos', '34916330', 'damianl657@gmail.com', 1, 'pepe', '02645671551', 3),
(2, 'rodriguez', 'uiuuiui', '7777777777', 'ddd@ddd.com', 1, 'horacio', '8787', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `division`
--

DROP TABLE IF EXISTS `division`;
CREATE TABLE IF NOT EXISTS `division` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `division`
--

INSERT INTO `division` (`id`, `nombre`) VALUES
(1, '1ro'),
(2, '2ndo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `genero`
--

DROP TABLE IF EXISTS `genero`;
CREATE TABLE IF NOT EXISTS `genero` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `genero`
--

INSERT INTO `genero` (`id`, `nombre`) VALUES
(1, 'Femenino'),
(2, 'Masculino'),
(3, 'No Binario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `item_venta`
--

DROP TABLE IF EXISTS `item_venta`;
CREATE TABLE IF NOT EXISTS `item_venta` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `estado` bigint(20) DEFAULT NULL,
  `precio_total` double DEFAULT NULL,
  `precio_id` bigint(20) DEFAULT NULL,
  `producto_id` bigint(20) DEFAULT NULL,
  `venta_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKyyaxr95ci05m75jftndll1` (`precio_id`),
  KEY `FK4vfin5d1lrbavmtaq76ah6qc2` (`producto_id`),
  KEY `FK6ixcdqqa8ren81ahfss5oy5ds` (`venta_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `item_venta`
--

INSERT INTO `item_venta` (`id`, `estado`, `precio_total`, `precio_id`, `producto_id`, `venta_id`) VALUES
(1, 1, 2000000, 1, 1, 1),
(2, 1, 2000000, 2, 2, 1),
(3, 1, 2000000, 1, 1, 2),
(4, 1, 2000000, 2, 2, 2),
(5, 1, 1000000, 4, 3, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

DROP TABLE IF EXISTS `marca`;
CREATE TABLE IF NOT EXISTS `marca` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `estado` bigint(20) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marca`
--

INSERT INTO `marca` (`id`, `estado`, `nombre`) VALUES
(1, 1, 'Samsung'),
(2, 1, 'LG'),
(3, 1, 'Lenovo'),
(4, 1, 'HP');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `placa_madre`
--

DROP TABLE IF EXISTS `placa_madre`;
CREATE TABLE IF NOT EXISTS `placa_madre` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cantidad_bancos_memorias` int(11) NOT NULL,
  `cantidad_bancospciexpress` int(11) NOT NULL,
  `marca` varchar(255) DEFAULT NULL,
  `modelo` varchar(255) DEFAULT NULL,
  `socket` varchar(255) DEFAULT NULL,
  `tipo_memoria` varchar(255) DEFAULT NULL,
  `estado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `placa_madre`
--

INSERT INTO `placa_madre` (`id`, `cantidad_bancos_memorias`, `cantidad_bancospciexpress`, `marca`, `modelo`, `socket`, `tipo_memoria`, `estado`) VALUES
(1, 2, 2, 'gigabyte2', 'gigabyte', '1170', 'ddr5', 1),
(3, 2, 2, 'gigabyte23', 'gigabyte', '1170', 'ddr5', 1),
(4, 2, 2, 'gigabyte', 'gigabyte', '1170', 'ddr5', 1),
(5, 2, 2, 'gigabyte', 'gigabyte', '1170', 'ddr5', 0),
(6, 2, 2, 'asus', 'noc', '1120', 'dd5', 0),
(7, 76, 876, 'LKJ', 'LKJ', 'LKJ', 'KJH', 0),
(8, 4, 2, 'MSI', 'MSI', '1170', 'DDR5', 1),
(9, 4, 2, 'MSI', 'MSI', '1170', 'DDR5', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `precio`
--

DROP TABLE IF EXISTS `precio`;
CREATE TABLE IF NOT EXISTS `precio` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `estado` bigint(20) DEFAULT NULL,
  `fecha` datetime(6) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `producto_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK64w86n0folwdyjw5d154b5mq` (`producto_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `precio`
--

INSERT INTO `precio` (`id`, `estado`, `fecha`, `precio`, `producto_id`) VALUES
(1, 1, '2024-08-22 00:00:00.000000', 2000000, 1),
(2, 1, '2024-08-22 00:00:00.000000', 2000000, 2),
(3, 1, '2024-08-22 00:00:00.000000', 1000000, 3),
(4, 1, '2024-08-22 00:00:00.000000', 1000000, 4),
(5, 1, '2024-08-22 00:00:00.000000', 500000, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

DROP TABLE IF EXISTS `producto`;
CREATE TABLE IF NOT EXISTS `producto` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) DEFAULT NULL,
  `estado` bigint(20) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `ultimo_precio_id` bigint(20) DEFAULT NULL,
  `tipo_producto_id` bigint(20) DEFAULT NULL,
  `marca_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK2em9dpm8qsg21wqf2v1bmshdw` (`tipo_producto_id`),
  KEY `FK868tnrt85f21kgcvt9bftgr8r` (`marca_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id`, `descripcion`, `estado`, `nombre`, `precio`, `ultimo_precio_id`, `tipo_producto_id`, `marca_id`) VALUES
(1, '32GB RAM I7 14TH', 1, NULL, 2000000, 1, 1, 1),
(2, '32GB RAM I7 14TH', 1, NULL, 2000000, 2, 1, 4),
(3, '55 PULGADAS WEB OS', 1, NULL, 1000000, 3, 2, 2),
(4, '55 PULGADAS ', 1, NULL, 1000000, 4, 2, 1),
(5, '24 PULGADAS 144HZ', 1, NULL, 500000, 5, 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_producto`
--

DROP TABLE IF EXISTS `tipo_producto`;
CREATE TABLE IF NOT EXISTS `tipo_producto` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `estado` bigint(20) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_producto`
--

INSERT INTO `tipo_producto` (`id`, `estado`, `nombre`) VALUES
(1, 1, 'Notebook'),
(2, 1, 'Televisor'),
(3, 1, 'Monitor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

DROP TABLE IF EXISTS `venta`;
CREATE TABLE IF NOT EXISTS `venta` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `estado` bigint(20) DEFAULT NULL,
  `fecha` datetime(6) DEFAULT NULL,
  `total` double DEFAULT NULL,
  `cliente_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKa7yaj59nfh3gft0h38o5bv472` (`cliente_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`id`, `estado`, `fecha`, `total`, `cliente_id`) VALUES
(1, 1, '2024-08-22 00:00:00.000000', 4000000, 2),
(2, 1, '2024-08-22 00:00:00.000000', 5000000, 1);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `alumnos`
--
ALTER TABLE `alumnos`
  ADD CONSTRAINT `FKhwsgk51d3cggvbaawwaba4asi` FOREIGN KEY (`division_modelo_id`) REFERENCES `division` (`id`),
  ADD CONSTRAINT `FKjx0qmy2g2c81yaja4ca9cnoli` FOREIGN KEY (`division_id`) REFERENCES `division` (`id`);

--
-- Filtros para la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `FK2jneo30tbxtors16jegervd50` FOREIGN KEY (`genero_id`) REFERENCES `genero` (`id`);

--
-- Filtros para la tabla `item_venta`
--
ALTER TABLE `item_venta`
  ADD CONSTRAINT `FK4vfin5d1lrbavmtaq76ah6qc2` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`),
  ADD CONSTRAINT `FK6ixcdqqa8ren81ahfss5oy5ds` FOREIGN KEY (`venta_id`) REFERENCES `venta` (`id`),
  ADD CONSTRAINT `FKyyaxr95ci05m75jftndll1` FOREIGN KEY (`precio_id`) REFERENCES `precio` (`id`);

--
-- Filtros para la tabla `precio`
--
ALTER TABLE `precio`
  ADD CONSTRAINT `FK64w86n0folwdyjw5d154b5mq` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`);

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `FK2em9dpm8qsg21wqf2v1bmshdw` FOREIGN KEY (`tipo_producto_id`) REFERENCES `tipo_producto` (`id`),
  ADD CONSTRAINT `FK868tnrt85f21kgcvt9bftgr8r` FOREIGN KEY (`marca_id`) REFERENCES `marca` (`id`);

--
-- Filtros para la tabla `venta`
--
ALTER TABLE `venta`
  ADD CONSTRAINT `FKa7yaj59nfh3gft0h38o5bv472` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
