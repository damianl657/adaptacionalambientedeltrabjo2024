package com.example.cursoSpringBootEpet4.repositorios;
import com.example.cursoSpringBootEpet4.modelo.Marca;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MarcaRepositorio extends JpaRepository<Marca, Long> {
    List<Marca> findByEstado(Long estado);
}
