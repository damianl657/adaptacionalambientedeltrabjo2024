package com.example.cursoSpringBootEpet4.controladores;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/java")
public class PracticaJava {
    @GetMapping("/practica1")
    public String practica1(Model modelo){
        System.out.println("-------------------");

        System.out.println("Hola Mundo");


        return "practicajava/practica1";
    }
    @GetMapping("/practicaAlumno")
    public String practicaAlumno(Model modelo){
        //no borrar este renglon. ES UN SEPARADOR PARA LA CONSOLA
        System.out.println("-------------------");
        System.out.println("Hola Mundo");
        System.out.println("Mi primer aplicacion en java");
        int a = 10;
        int b = 50;
        int suma = a + b;
        System.out.println("la suma entre a y b es: "+suma);

        List<Integer> lista = new ArrayList<>();

        lista.add(5);
        lista.add(10);
        lista.add(45);
        for(Integer instancia : lista){
            System.out.println("Valor :"+instancia);
        }




        return "practicajava/practica1";
    }
}

