package com.example.cursoSpringBootEpet4.repositorios;

import com.example.cursoSpringBootEpet4.modelo.Venta;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VentaRepositorio  extends JpaRepository<Venta, Long> {
    List<Venta> findByEstado(Long estado);
}
