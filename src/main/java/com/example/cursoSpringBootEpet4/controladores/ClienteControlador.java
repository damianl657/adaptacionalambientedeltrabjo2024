package com.example.cursoSpringBootEpet4.controladores;

import com.example.cursoSpringBootEpet4.modelo.ClienteModelo;
import com.example.cursoSpringBootEpet4.modelo.GeneroModelo;
import com.example.cursoSpringBootEpet4.modelo.PlacaMadreModelo;
import com.example.cursoSpringBootEpet4.repositorios.ClienteRepositorio;
import com.example.cursoSpringBootEpet4.repositorios.GeneroRepository;
import com.example.cursoSpringBootEpet4.repositorios.PlacaMadreRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/clientes")
public class ClienteControlador {
    @Autowired
    private ClienteRepositorio clienteRepositorio;
    @Autowired
    private GeneroRepository generoRepository;
    @GetMapping({"","/"})
    public String principal(Model modelo){
        List<ClienteModelo> clienteModeloArrayList = new ArrayList<>();
        clienteModeloArrayList = clienteRepositorio.findByEstadoOrderByNombreAsc(1L);
        modelo.addAttribute("clientes",clienteModeloArrayList);
        return "cliente/principal";
    }
    @GetMapping("/alta")
    public String alta(Model modelo){
        ClienteModelo clienteModelo = new ClienteModelo();
        List<GeneroModelo> generos = new ArrayList<>();
        generos = this.generoRepository.findAll();
        modelo.addAttribute("cliente",clienteModelo);
        modelo.addAttribute("generos",generos);
        return "cliente/alta";
    }
    @PostMapping("/alta")
    public String alta(@ModelAttribute("cliente") ClienteModelo clienteModelo, Model modelo){
        clienteModelo.setEstado(1L);
        clienteRepositorio.save(clienteModelo);
        return "redirect:/clientes";
    }
    @GetMapping("/eliminar/{id}")
    public String eliminar(@PathVariable Long id){
        ClienteModelo clienteModelo = this.clienteRepositorio.findById(id).get();
        clienteModelo.setEstado(0L);
        this.clienteRepositorio.save(clienteModelo);
        return "redirect:/clientes";
    }
    @GetMapping("/editar/{id}")
    public String editar(@PathVariable Long id, Model modelo){
        ClienteModelo clienteModelo = this.clienteRepositorio.findById(id).get();
        List<GeneroModelo> generos = new ArrayList<>();
        generos = this.generoRepository.findAll();
        modelo.addAttribute("generos",generos);
        modelo.addAttribute("cliente",clienteModelo);
        return "cliente/editar";
    }
    @PostMapping("/editar")
    public String editar(Model modelo,@ModelAttribute("cliente") ClienteModelo clienteModelo){
        System.out.println(clienteModelo.getId());
        clienteModelo.setEstado(1L);
        this.clienteRepositorio.save(clienteModelo);
        return "redirect:/clientes";
    }
}
