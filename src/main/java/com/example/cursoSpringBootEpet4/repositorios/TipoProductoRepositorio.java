package com.example.cursoSpringBootEpet4.repositorios;

import com.example.cursoSpringBootEpet4.modelo.TipoProducto;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TipoProductoRepositorio  extends JpaRepository<TipoProducto, Long> {
    List<TipoProducto> findByEstado(Long estado);
}
