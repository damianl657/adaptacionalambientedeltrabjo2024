package com.example.cursoSpringBootEpet4.controladores;

import com.example.cursoSpringBootEpet4.modelo.PlacaMadreModelo;
import com.example.cursoSpringBootEpet4.repositorios.PlacaMadreRepositorio;
import com.example.cursoSpringBootEpet4.utilidades.PlacaMadre;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/placa-madre")
public class PlacaMadreControlador {
    @Autowired
    private PlacaMadreRepositorio placaMadreRepositorio;
    @GetMapping({"","/"})
    public String principal(Model modelo){
        List<PlacaMadreModelo> placaMadreModeloList = new ArrayList<>();
        placaMadreModeloList = placaMadreRepositorio.findByEstado(1L);
        modelo.addAttribute("listaPlacaMadre",placaMadreModeloList);
        return "placamadre/principal";
    }
    @GetMapping("/alta")
    public String altaPlacaMadre(Model modelo){
        PlacaMadreModelo placaMadreModelo = new PlacaMadreModelo();
        modelo.addAttribute("placaMadre",placaMadreModelo);
        return "placamadre/altaPlacaMadre";
    }
    @PostMapping("/alta")
    public String mostrarPlacaMadre(@ModelAttribute("placaMadre") PlacaMadreModelo placaMadreModelo, Model modelo){
        PlacaMadreModelo placaMadreModeloSaved = new PlacaMadreModelo();
        placaMadreModelo.setEstado(1L);
        placaMadreModeloSaved = placaMadreRepositorio.save(placaMadreModelo);
        return "redirect:/placa-madre";
        //modelo.addAttribute("placaMadre",placaMadreModeloSaved);
        //return "placamadre/mostrarPlacaMadre";
    }
    @GetMapping("/eliminar/{id}")
    public String eliminar(@PathVariable Long id){
        PlacaMadreModelo placaMadreModelo = this.placaMadreRepositorio.findById(id).get();
        placaMadreModelo.setEstado(0L);
        this.placaMadreRepositorio.save(placaMadreModelo);
        return "redirect:/placa-madre";
    }
    @GetMapping("/editar/{id}")
    public String editar(@PathVariable Long id, Model modelo){
        PlacaMadreModelo placaMadreModelo = this.placaMadreRepositorio.findById(id).get();
        modelo.addAttribute("placaMadre",placaMadreModelo);
        return "placamadre/editarPlacaMadre";
    }
    @PostMapping("/editar")
    public String editar(Model modelo,@ModelAttribute("placaMadre") PlacaMadreModelo placaMadreModelo){
        System.out.println(placaMadreModelo.getId());
        placaMadreModelo.setEstado(1L);
        //System.out.println(placaMadreModelo.getMarca());
        this.placaMadreRepositorio.save(placaMadreModelo);
        return "redirect:/placa-madre";
    }
}
